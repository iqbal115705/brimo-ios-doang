import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('Wallet New Form/XCUIElementTypeStaticText - Dompet Digital'), 0)

CustomKeywords.'screenshot.capture.Screenshot'()

if(page_condition.toString() == 'no internet') {
	
	Mobile.toggleAirplaneMode("true")
	
	Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Lanjutkan'), 0)
	
	Mobile.toggleAirplaneMode("false")
	
	Mobile.verifyElementExist(findTestObject('Pop Up no Internet/XCUIElementTypeStaticText - Tidak Ada Koneksi Internet'), 0)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
	Mobile.tap(findTestObject('Object Repository/Pop Up no Internet/XCUIElementTypeStaticText - button baiklah no connection'), 0)
	
	
} else if(page_condition.toString() == 'out session') {
	
	Mobile.delay(420)
	
	Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Lanjutkan'), 0)
	
	Mobile.delay(1)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
	Mobile.verifyElementExist(findTestObject('Fast Menu/XCUIElementTypeButton - Login'), 0)
	
} else {
	
	Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Lanjutkan'), 0)
	
	Mobile.delay(1)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
}
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('PFM/tambah pengeluaran/XCUIElementTypeTextField - Rp'), 10)

Mobile.sendKeys(findTestObject('PFM/tambah pengeluaran/XCUIElementTypeTextField - input jumlah', [(1) : 123456789010]), amount)

nominal = Mobile.getText(findTestObject('PFM/tambah pengeluaran/XCUIElementTypeTextField - Rp'), 0)

String digitNominal = nominal.replaceAll("\\D+", "")

if(digitNominal.length() <= 10) {
	KeywordUtil.markPassed('digit jumlah kurang dari 11 digit')
} else {
	KeywordUtil.markError('error lebih dari 10 digit')
}

CustomKeywords.'screenshot.capture.Screenshot'()


Feature: ATM Location
  @Valid
  Scenario Outline: User find ATM BRI location
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want use account feature
    And I need info about ATM BRI location
    Then I got info about ATM BRI location
    Examples: 
      | username   | password   |
      | taufan123456 | Jakarta123 |
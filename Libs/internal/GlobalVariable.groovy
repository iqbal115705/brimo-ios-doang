package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object identifierApp
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object dbUrl
     
    /**
     * <p></p>
     */
    public static Object dbName
     
    /**
     * <p></p>
     */
    public static Object dbPort
     
    /**
     * <p></p>
     */
    public static Object dbUsername
     
    /**
     * <p></p>
     */
    public static Object dbPassword
     
    /**
     * <p></p>
     */
    public static Object timeoutShort
     
    /**
     * <p></p>
     */
    public static Object timeoutMiddle
     
    /**
     * <p></p>
     */
    public static Object destinationPhone
     
    /**
     * <p></p>
     */
    public static Object nominalPulsa
     
    /**
     * <p></p>
     */
    public static Object destinationBank
     
    /**
     * <p></p>
     */
    public static Object destinationAccount
     
    /**
     * <p></p>
     */
    public static Object nominalTransfer
     
    /**
     * <p></p>
     */
    public static Object detailTransfer
     
    /**
     * <p></p>
     */
    public static Object deviceHeight
     
    /**
     * <p></p>
     */
    public static Object typeGopay
     
    /**
     * <p></p>
     */
    public static Object screenshot
     
    /**
     * <p></p>
     */
    public static Object dbHistory2
     
    /**
     * <p></p>
     */
    public static Object dbHistory10
     
    /**
     * <p></p>
     */
    public static Object accountNumber
     
    /**
     * <p></p>
     */
    public static Object dateIndo
     
    /**
     * <p></p>
     */
    public static Object element
     
    /**
     * <p></p>
     */
    public static Object walletOption
     
    /**
     * <p></p>
     */
    public static Object asd
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            identifierApp = selectedVariables['identifierApp']
            username = selectedVariables['username']
            password = selectedVariables['password']
            dbUrl = selectedVariables['dbUrl']
            dbName = selectedVariables['dbName']
            dbPort = selectedVariables['dbPort']
            dbUsername = selectedVariables['dbUsername']
            dbPassword = selectedVariables['dbPassword']
            timeoutShort = selectedVariables['timeoutShort']
            timeoutMiddle = selectedVariables['timeoutMiddle']
            destinationPhone = selectedVariables['destinationPhone']
            nominalPulsa = selectedVariables['nominalPulsa']
            destinationBank = selectedVariables['destinationBank']
            destinationAccount = selectedVariables['destinationAccount']
            nominalTransfer = selectedVariables['nominalTransfer']
            detailTransfer = selectedVariables['detailTransfer']
            deviceHeight = selectedVariables['deviceHeight']
            typeGopay = selectedVariables['typeGopay']
            screenshot = selectedVariables['screenshot']
            dbHistory2 = selectedVariables['dbHistory2']
            dbHistory10 = selectedVariables['dbHistory10']
            accountNumber = selectedVariables['accountNumber']
            dateIndo = selectedVariables['dateIndo']
            element = selectedVariables['element']
            walletOption = selectedVariables['walletOption']
            asd = selectedVariables['asd']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
